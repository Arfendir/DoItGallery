package com.dev.doitgallery.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dev.doitgallery.R;
import com.dev.doitgallery.model.Image;

import java.util.ArrayList;

/**
 * Created by Evgeniy on 01.09.2017.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Image> images;

    private static final String TAG = ImagesAdapter.class.getSimpleName();

    public ImagesAdapter(Context context, ArrayList<Image> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Image image = images.get(position);

        Glide.with(context).load(image.getSmallImagePath()).into(holder.pictureImageView);
        holder.weatherTextView.setText(image.getParameters().getWeather());
        holder.locationTextView.setText(image.getParameters().getAddress());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(context);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                View dialogView = ((AppCompatActivity) context).getLayoutInflater().inflate(R.layout.image_layout, null);
                ImageView picture = dialogView.findViewById(R.id.image);
                Glide.with(context).load(image.getSmallImagePath()).asBitmap().into(picture);
                TextView weather = dialogView.findViewById(R.id.weather);
                TextView location = dialogView.findViewById(R.id.location);
                weather.setText(image.getParameters().getWeather());
                location.setText(image.getParameters().getAddress());
                dialog.setContentView(dialogView);
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pictureImageView;
        TextView weatherTextView;
        TextView locationTextView;

        ViewHolder(View itemView) {
            super(itemView);
            pictureImageView = itemView.findViewById(R.id.image);
            weatherTextView = itemView.findViewById(R.id.weather);
            locationTextView = itemView.findViewById(R.id.location);
        }
    }
}
