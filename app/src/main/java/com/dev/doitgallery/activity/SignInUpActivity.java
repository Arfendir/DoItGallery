package com.dev.doitgallery.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.doitgallery.R;
import com.dev.doitgallery.model.AuthObject;
import com.dev.doitgallery.server.ApiService;
import com.dev.doitgallery.server.RetrofitApi;
import com.dev.doitgallery.utils.SharedPrefsUtil;
import com.dev.doitgallery.utils.Utils;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dev.doitgallery.activity.AddImageActivity.READ_EXT_PERM;

public class SignInUpActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    public static final String TOKEN = "token";
    private static final String TAG = SignInUpActivity.class.getSimpleName();
    private static final int SELECT_AVATAR = 1234;

    private ApiService apiService;
    private Uri selectedAvatarURI;

    @BindView(R.id.avatar)
    ImageView avatarImageView;

    @BindView(R.id.username_layout)
    TextInputLayout userNameLayout;

    @BindView(R.id.sign_up_username)
    EditText userNameEditText;

    @BindView(R.id.sign_in_email)
    EditText emailEditText;

    @BindView(R.id.sign_in_password)
    EditText passwordEditText;

    @BindView(R.id.sign_in_button)
    Button signInButton;

    @BindView(R.id.sign_up_button)
    Button signUpButton;

    @BindView(R.id.sign_in_link)
    TextView signInLinkTextView;

    @BindView(R.id.sign_up_link)
    TextView signUpLinkTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_up);
        ButterKnife.bind(this);

        if (!SharedPrefsUtil.getStringData(this, TOKEN).isEmpty()) {
            displaySignInFields();
        } else {
            displaySignUpFields();
        }

        Utils.hideKeyboardOnTouch(findViewById(R.id.frame_window), this);

        apiService = RetrofitApi.getInstance().getApiService();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_AVATAR) {
            if (resultCode == Activity.RESULT_OK) {
                selectedAvatarURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedAvatarURI);
                    avatarImageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    Log.e(TAG, "Error " + e.getMessage());
                }
            }
        }
    }

    @OnClick(R.id.avatar)
    public void onClick() {
        chooseAvatar();
    }

    @OnClick(R.id.sign_in_link)
    public void signInLinkClick() {
        displaySignInFields();
        clearFields();
    }

    @OnClick(R.id.sign_up_link)
    public void signUpLinkClick() {
        displaySignUpFields();
        clearFields();
    }

    /**
     * Display fields for sign up logic.
     */
    private void displaySignUpFields() {
        avatarImageView.setVisibility(View.VISIBLE);
        userNameLayout.setVisibility(View.VISIBLE);
        signInButton.setVisibility(View.GONE);
        signUpButton.setVisibility(View.VISIBLE);
        signInLinkTextView.setVisibility(View.VISIBLE);
        signUpLinkTextView.setVisibility(View.GONE);
    }

    /**
     * Display fields for sign in logic.
     */
    private void displaySignInFields() {
        avatarImageView.setVisibility(View.GONE);
        userNameLayout.setVisibility(View.GONE);
        signInButton.setVisibility(View.VISIBLE);
        signUpButton.setVisibility(View.GONE);
        signInLinkTextView.setVisibility(View.GONE);
        signUpLinkTextView.setVisibility(View.VISIBLE);
    }

    /**
     * Clear login and password fields.
     */
    private void clearFields() {
        emailEditText.setText("");
        passwordEditText.setText("");
        userNameEditText.setText("");
        avatarImageView.setImageDrawable(getResources().getDrawable(R.drawable.placeholder));
    }

    @OnClick(R.id.sign_in_button)
    public void signInOnClick() {
        apiService = RetrofitApi.getInstance().getApiService();

        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.error_field_required));
            focusView = emailEditText;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            emailEditText.setError(getString(R.string.error_invalid_email));
            focusView = emailEditText;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (Utils.isConnectedToNetwork(this)) {
                signIn(email, password);
            } else {
                Toast.makeText(this, R.string.enable_internet, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.sign_up_button)
    public void signUpOnClick() {
        apiService = RetrofitApi.getInstance().getApiService();

        String email = emailEditText.getText().toString();
        String username = userNameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.error_field_required));
            focusView = emailEditText;
            cancel = true;
        } else if (!Utils.isEmailValid(email)) {
            emailEditText.setError(getString(R.string.error_invalid_email));
            focusView = emailEditText;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if (selectedAvatarURI != null) {
                MultipartBody.Part avatar = Utils.prepareFilePart(this, "avatar", selectedAvatarURI);
                if (Utils.isConnectedToNetwork(this)) {
                    signUp(username, email, password, avatar);
                } else {
                    Toast.makeText(this, R.string.enable_internet, Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, R.string.error_pick_avatar, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Sign in to server
     *
     * @param email
     * @param password
     */
    private void signIn(String email, String password) {
        Call<AuthObject> signInCall = apiService.signIn(email, password);
        findViewById(R.id.progress_layout).setVisibility(View.VISIBLE);
        signInCall.enqueue(new Callback<AuthObject>() {
            @Override
            public void onResponse(Call<AuthObject> call, Response<AuthObject> response) {
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    final AuthObject responseData = response.body();
                    Log.d(TAG, "SignIn " + response.body());
                    SharedPrefsUtil.putStringData(SignInUpActivity.this, TOKEN, responseData.getToken());

                    Intent intent = new Intent(SignInUpActivity.this, ImageGalleryActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    try {
                        String error = response.errorBody().string();
                        Log.e(TAG, "Response error " + error);
                        Toast.makeText(SignInUpActivity.this, error, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException Error " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthObject> call, Throwable t) {
                Log.d(TAG, "Error " + t.getMessage());
                Toast.makeText(SignInUpActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
            }
        });
    }

    /**
     * Sign up to server
     *
     * @param username not required
     * @param email
     * @param password
     * @param avatar
     */
    private void signUp(String username, String email, String password, MultipartBody.Part avatar) {
        Log.d(TAG, "Params " + username + " " + email + " " + password + " " + avatar);

        RequestBody usernameBody = RequestBody.create(MediaType.parse("text/plain"), username);
        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody passwordBody = RequestBody.create(MediaType.parse("text/plain"), password);

        Call<ResponseBody> signUpCall = apiService.signUp(usernameBody, emailBody, passwordBody, avatar);
        findViewById(R.id.progress_layout).setVisibility(View.VISIBLE);
        signUpCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    final ResponseBody responseData = response.body();
                    try {
                        String str = responseData.string();
                        Log.d(TAG, "SignUp " + str);
                        Toast.makeText(SignInUpActivity.this, str, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException error " + e.getMessage());
                    }
                    displaySignInFields();
                } else {
                    try {
                        String error = response.errorBody().string();
                        Log.e(TAG, "Response error " + error);
                        Toast.makeText(SignInUpActivity.this, error, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException error " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
                Toast.makeText(SignInUpActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
            }
        });
    }

    @AfterPermissionGranted(READ_EXT_PERM)
    public void chooseAvatar() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_pic)), SELECT_AVATAR);
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    "Read external storage permission",
                    READ_EXT_PERM,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show();
    }
}
