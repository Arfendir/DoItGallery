package com.dev.doitgallery.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dev.doitgallery.R;
import com.dev.doitgallery.server.ApiService;
import com.dev.doitgallery.server.RetrofitApi;
import com.dev.doitgallery.utils.SharedPrefsUtil;
import com.dev.doitgallery.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dev.doitgallery.activity.SignInUpActivity.TOKEN;

public class AddImageActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    public static final int READ_EXT_PERM = 222;
    private static final int LOCATION_PERM = 111;
    private static final String TAG = AddImageActivity.class.getSimpleName();
    private static final int SELECT_IMAGE = 5678;
    private Uri selectedImageURI;
    private LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationClient;
    private double latitude;
    private double longitude;

    @BindView(R.id.image)
    ImageView pictureImageView;

    @BindView(R.id.description)
    EditText descriptionEditText;

    @BindView(R.id.hashtag)
    EditText hashTagEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);

        Utils.hideKeyboardOnTouch(findViewById(R.id.frame_window), this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_image_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_tick) {
            if (selectedImageURI != null) {
                setLocationPerm();
            } else {
                Toast.makeText(this, R.string.error_pick_image, Toast.LENGTH_LONG).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.image)
    public void choosePicture() {
        readExternal();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                selectedImageURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageURI);
                    pictureImageView.setImageBitmap(bitmap);
                } catch (IOException e) {
                    Log.e(TAG, "IOException Error " + e.getMessage());
                }
            }
        }
    }

    @AfterPermissionGranted(READ_EXT_PERM)
    public void readExternal() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_pic)), SELECT_IMAGE);
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    "Read external storage permission",
                    READ_EXT_PERM,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @AfterPermissionGranted(LOCATION_PERM)
    public void setLocationPerm() {

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isNetworkEnabled) {
            showGPSDisabledAlertToUser();
        } else {

            boolean coarseLocation = EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (coarseLocation) {
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                    MultipartBody.Part image = Utils.prepareFilePart(AddImageActivity.this, "image", selectedImageURI);
                                    addNewImage(image);
                                } else {
                                    Toast.makeText(AddImageActivity.this, R.string.location_not_determined, Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            } else {
                EasyPermissions.requestPermissions(
                        this,
                        "Location",
                        LOCATION_PERM,
                        Manifest.permission.ACCESS_COARSE_LOCATION);
            }
        }
    }

    /**
     * Send image file to server, description and hashtag not required
     *
     * @param image
     */
    private void addNewImage(MultipartBody.Part image) {
        ApiService apiService = RetrofitApi.getInstance().getApiService();

        String description = descriptionEditText.getText().toString();
        String hashTag = hashTagEditText.getText().toString();

        Log.d(TAG, "Params " + SharedPrefsUtil.getStringData(this, TOKEN) + " " + image + " " + description + " " + hashTag + " " + latitude + " " + longitude);
        if (Utils.isConnectedToNetwork(this)) {
            Call<ResponseBody> addImageCall = apiService.addNewImage(SharedPrefsUtil.getStringData(this, TOKEN), image, description, hashTag, latitude, longitude);
            findViewById(R.id.progress_layout).setVisibility(View.VISIBLE);
            addImageCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    findViewById(R.id.progress_layout).setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        ResponseBody responseData = response.body();
                        Log.d(TAG, "New Image " + responseData);

                        Intent intent = new Intent(AddImageActivity.this, ImageGalleryActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        try {
                            String errorResponse = response.errorBody().string();
                            String errorObject = new JSONObject(errorResponse)
                                    .getJSONObject("children").getJSONObject("image").getJSONArray("errors").toString();
                            Log.e(TAG, "Response error " + errorResponse);
                            Toast.makeText(AddImageActivity.this, errorObject, Toast.LENGTH_LONG).show();
                        } catch (IOException | JSONException e) {
                            Log.e(TAG, "IOException error " + e.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "Error " + t.getMessage());
                    Toast.makeText(AddImageActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                    findViewById(R.id.progress_layout).setVisibility(View.GONE);
                }
            });
        } else {
            Toast.makeText(this, R.string.enable_internet, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Show settings to enable gps
     */
    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.gps_disabled)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                            }
                        });
        builder.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder.create().show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, ImageGalleryActivity.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show();
    }
}
