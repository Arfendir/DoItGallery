package com.dev.doitgallery.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dev.doitgallery.R;
import com.dev.doitgallery.adapter.ImagesAdapter;
import com.dev.doitgallery.model.GIFimage;
import com.dev.doitgallery.model.Image;
import com.dev.doitgallery.model.Images;
import com.dev.doitgallery.server.ApiService;
import com.dev.doitgallery.server.RetrofitApi;
import com.dev.doitgallery.utils.SharedPrefsUtil;
import com.dev.doitgallery.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dev.doitgallery.activity.SignInUpActivity.TOKEN;

public class ImageGalleryActivity extends AppCompatActivity {

    private static final String TAG = ImageGalleryActivity.class.getSimpleName();
    private RecyclerView imagesRecyclerView;
    private ImagesAdapter adapter;
    private ArrayList<Image> images;
    private String gifImageUrl;
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_gallery);

        imagesRecyclerView = findViewById(R.id.images_list);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        imagesRecyclerView.setLayoutManager(mLayoutManager);
        imagesRecyclerView.addItemDecoration(new SpacesItemDecoration(5));
        imagesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        imagesRecyclerView.setAdapter(adapter);

        apiService = RetrofitApi.getInstance().getApiService();
        if (Utils.isConnectedToNetwork(this)) {
            getImages(apiService);
        } else {
            Toast.makeText(this, R.string.enable_internet, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gallery_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_show_gif) {
            if (Utils.isConnectedToNetwork(this)) {
                getGif();
            } else {
                Toast.makeText(this, R.string.enable_internet, Toast.LENGTH_LONG).show();
            }
            return true;
        } else if (id == R.id.action_add_image) {
            Intent intent = new Intent(ImageGalleryActivity.this, AddImageActivity.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;

            if (parent.getChildLayoutPosition(view) == 0) {
                outRect.top = space;
            } else {
                outRect.top = 0;
            }
        }
    }

    /**
     * Get images from server and populate them in recyclerview
     *
     * @param apiService
     */
    private void getImages(ApiService apiService) {
        String token = SharedPrefsUtil.getStringData(this, TOKEN);
        Log.d(TAG, "Token " + token);
        Call<Images> imagesCall = apiService.getAllImages(token);
        findViewById(R.id.progress_layout).setVisibility(View.VISIBLE);
        imagesCall.enqueue(new Callback<Images>() {
            @Override
            public void onResponse(Call<Images> call, Response<Images> response) {
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Images responseData = response.body();
                    Log.d(TAG, "Images " + response.body());
                    images = responseData.getImages();

                    adapter = new ImagesAdapter(ImageGalleryActivity.this, images);
                    imagesRecyclerView.setAdapter(adapter);
                } else {
                    try {
                        Log.e(TAG, "Response error " + response.errorBody().string());
                        Toast.makeText(ImageGalleryActivity.this, response.errorBody().string(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException error " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<Images> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
                Toast.makeText(ImageGalleryActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
            }
        });
    }

    /**
     * Show gif in dialog
     */
    private void getGif() {
        Call<GIFimage> gifCall = apiService.getGIF(SharedPrefsUtil.getStringData(this, TOKEN), "");
        findViewById(R.id.progress_layout).setVisibility(View.VISIBLE);
        gifCall.enqueue(new Callback<GIFimage>() {
            @Override
            public void onResponse(Call<GIFimage> call, Response<GIFimage> response) {
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    GIFimage responseData = response.body();
                    Log.d(TAG, "GIF " + response.body());
                    gifImageUrl = responseData.getGif();
                    Dialog dialog = new Dialog(ImageGalleryActivity.this);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    View view = getLayoutInflater().inflate(R.layout.gif_layout, null);
                    ImageView picture = view.findViewById(R.id.dialog_image);
                    Glide.with(ImageGalleryActivity.this).load(gifImageUrl).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(picture);
                    dialog.setContentView(view);
                    dialog.show();
                } else {
                    Log.e(TAG, "Response error " + response.errorBody().toString());
                    Toast.makeText(ImageGalleryActivity.this, response.errorBody().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<GIFimage> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
                Toast.makeText(ImageGalleryActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                findViewById(R.id.progress_layout).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, SignInUpActivity.class));
    }
}
