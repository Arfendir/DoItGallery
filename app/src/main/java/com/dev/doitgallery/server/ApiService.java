package com.dev.doitgallery.server;

import com.dev.doitgallery.model.AuthObject;
import com.dev.doitgallery.model.GIFimage;
import com.dev.doitgallery.model.Images;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("login")
    Call<AuthObject> signIn(@Field("email") String email,
                            @Field("password") String password);

    @Multipart
    @POST("create")
    Call<ResponseBody> signUp(@Part("username") RequestBody username,
                            @Part("email") RequestBody email,
                            @Part("password") RequestBody password,
                            @Part MultipartBody.Part file);

    @Headers("Content-Type: application/json")
    @GET("all")
    Call<Images> getAllImages(@Header("token") String token);

    @Headers("Content-Type: application/json")
    @GET("gif")
    Call<GIFimage> getGIF(@Header("token") String token, @Query("weather") String weather);

    @Multipart
    @POST("image")
    Call<ResponseBody> addNewImage(@Header("token") String token,
                                   @Part MultipartBody.Part file,
                                   @Part("description") String description,
                                   @Part("hashtag") String hashtag,
                                   @Part("latitude") double latitude,
                                   @Part("longitude") double longitude);
}