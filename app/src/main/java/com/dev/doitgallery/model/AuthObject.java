package com.dev.doitgallery.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Evgeniy on 06.10.2017.
 */

public class AuthObject {

    @SerializedName("creation_time")
    private String creationTime;
    private String token;
    private String avatar;

    public AuthObject() {
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "AuthObject{" +
                "creationTime='" + creationTime + '\'' +
                ", token='" + token + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
