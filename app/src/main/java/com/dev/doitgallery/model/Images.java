package com.dev.doitgallery.model;

import java.util.ArrayList;

/**
 * Created by Evgeniy on 06.10.2017.
 */

public class Images {

    private ArrayList<Image> images;

    public Images() {
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "Images{" +
                "images=" + images +
                '}';
    }
}
