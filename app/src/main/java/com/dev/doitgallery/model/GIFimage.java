package com.dev.doitgallery.model;

/**
 * Created by Evgeniy on 06.10.2017.
 */

public class GIFimage {

    private String gif;

    public GIFimage() {
    }

    public String getGif() {
        return gif;
    }

    public void setGif(String gif) {
        this.gif = gif;
    }

    @Override
    public String toString() {
        return "GIFimage{" +
                "gif='" + gif + '\'' +
                '}';
    }
}
