package com.dev.doitgallery.model;

/**
 * Created by Evgeniy on 06.10.2017.
 */

public class Image {

    private int id;
    private String description;
    private String hashtag;
    private Parameters parameters;
    private String bigImagePath;
    private String smallImagePath;

    public Image() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public String getBigImagePath() {
        return bigImagePath;
    }

    public void setBigImagePath(String bigImagePath) {
        this.bigImagePath = bigImagePath;
    }

    public String getSmallImagePath() {
        return smallImagePath;
    }

    public void setSmallImagePath(String smallImagePath) {
        this.smallImagePath = smallImagePath;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", hashtag='" + hashtag + '\'' +
                ", parameters=" + parameters +
                ", bigImagePath='" + bigImagePath + '\'' +
                ", smallImagePath='" + smallImagePath + '\'' +
                '}';
    }
}
