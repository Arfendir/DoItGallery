package com.dev.doitgallery.model;

/**
 * Created by Evgeniy on 06.10.2017.
 */

public class Parameters {

    private String address;
    private String weather;

    public Parameters() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    @Override
    public String toString() {
        return "Parameters{" +
                "address='" + address + '\'' +
                ", weather='" + weather + '\'' +
                '}';
    }
}
